#ifndef INCLUDE_FIX16_H
#define INCLUDE_FIX16_H

#include <stddef.h>
#include <stdint.h>
#include <string.h>

typedef int16_t fix16_rot_256;
typedef int16_t fix16_unit_64;

static const fix16_rot_256 fix16_rot_256_quarter = 64;
static const fix16_unit_64 fix16_unit_64_one = 64;

#include "fix16_tables.h"

static inline fix16_unit_64 fix16_sin(fix16_rot_256 angle) {
    uint16_t offset = (uint16_t) angle;
    return sin_table[offset];
}

static inline fix16_unit_64 fix16_cos(fix16_rot_256 angle) {
    uint16_t offset = (uint16_t) (angle + fix16_rot_256_quarter);
    return sin_table[offset];
}

static inline fix16_rot_256 fix16_asin(fix16_unit_64 value) {
    uint16_t offset = (uint16_t) value;
    return asin_table[offset];
}

static inline fix16_rot_256 fix16_acos(fix16_unit_64 value) {
    uint16_t offset = (uint16_t) -value;
    return asin_table[offset] + fix16_rot_256_quarter;
}

static inline fix16_unit_64 fix16_mul(fix16_unit_64 a, fix16_unit_64 b) {
    int32_t c = (int32_t) a * (int32_t) b;
    c /= fix16_unit_64_one;
    return c;
}

static inline fix16_unit_64 fix16_div(fix16_unit_64 a, fix16_unit_64 b) {
    fix16_unit_64 c = (int32_t) a * fix16_unit_64_one / b;
    return c;
}

static inline void fix16_mul_matrix(
    const size_t rows,
    const size_t cols,
    const size_t dim3,
    fix16_unit_64 a[rows][dim3],
    fix16_unit_64 b[dim3][cols],
    fix16_unit_64 c[rows][cols]) {
    fix16_unit_64 c_tmp[rows][cols];
    for (size_t i = 0; i < rows; ++i) {
        for (size_t j = 0; j < cols; ++j) {
            c_tmp[i][j] = 0;
            for (size_t k = 0; k < dim3; ++k) {
                c_tmp[i][j] += fix16_mul(a[i][k], b[k][j]);
            }
        }
    }
    memcpy(c, c_tmp, sizeof(c_tmp));
}

#endif
