#include "vgfx.h"
#include "v99x8.h"

// Uses GRAPHIC7

void vgfx_screen_draw_line(fix16_unit_64 x_0, fix16_unit_64 y_0, fix16_unit_64 x_1, fix16_unit_64 y_1, uint8_t colour) {
    enum v99x8_argument arg = V99X8_ARGUMENT_MXD_VRAM;

    // TODO: Should this round?
    long x_0_screen = x_0 / fix16_unit_64_one;
    long x_1_screen = x_1 / fix16_unit_64_one;
    long y_0_screen = y_0 / fix16_unit_64_one;
    long y_1_screen = y_1 / fix16_unit_64_one;

    long maj = x_1_screen - x_0_screen;
    long min = y_1_screen - y_0_screen;

    if (maj < 0) {
        maj = -maj;
        arg |= V99X8_ARGUMENT_DIX_LEFT;
    } else {
        arg |= V99X8_ARGUMENT_DIX_RIGHT;
    }

    if (min < 0) {
        min = -min;
        arg |= V99X8_ARGUMENT_DIY_UP;
    } else {
        arg |= V99X8_ARGUMENT_DIY_DOWN;
    }

    if (maj < min) {
        long tmp = maj;
        maj = min;
        min = tmp;
        arg |= V99X8_ARGUMENT_MAJ_LONG_Y;
    } else {
        arg |= V99X8_ARGUMENT_MAJ_LONG_X;
    }

    v99x8_line(x_0_screen, y_0_screen, maj, min, colour, arg, V99X8_LOGICAL_OPERATION_IMP);
}

static inline long scale_coord(long v, long scale, long v_center, long scale_center) {
    return (v - v_center) * scale / scale_center + v_center;
}

void vgfx_line_draw(struct vgfx_line *line, uint8_t colour) {
    fix16_unit_64 a[4][1];
    fix16_unit_64 b[4][1];

    vgfx_point_to_position(&line->a, a);
    vgfx_point_to_position(&line->b, b);

    fix16_unit_64 persp[4][4];
    vgfx_matrix_perspective(persp);
    fix16_unit_64 transl1[4][4];
    vgfx_matrix_translation(-128 * fix16_unit_64_one, -106 * fix16_unit_64_one, 0, transl1);
    fix16_unit_64 transl2[4][4];
    vgfx_matrix_translation(128 * fix16_unit_64_one, 106 * fix16_unit_64_one, 0, transl2);

    fix16_mul_matrix(4, 4, 4, persp, transl1, persp);
    fix16_mul_matrix(4, 4, 4, transl2, persp, persp);

    fix16_mul_matrix(4, 1, 4, persp, a, a);
    fix16_mul_matrix(4, 1, 4, persp, b, b);

    vgfx_homogeneous_divide(a, a);
    vgfx_homogeneous_divide(b, b);

    vgfx_screen_draw_line(a[0][0], a[1][0], b[0][0], b[1][0], colour);
}

void vgfx_bitmap_to_spritemap_16(const uint16_t bitmap[16], uint8_t spritemap[4][8]) {
    for (unsigned block = 0; block < 4; ++block) {
        for (unsigned row_in_block = 0; row_in_block < 8; ++row_in_block) {
            unsigned effective_row = row_in_block + (block & 0x1 ? 8 : 0);
            spritemap[block][row_in_block] = (bitmap[effective_row] >> (block & 0x2 ? 0 : 8)) & 0xFF;
        }
    }
}

void vgfx_bitmap_to_spritemap_32(const uint32_t bitmap[32], uint8_t spritemap[4][4][8]) {
    for (unsigned superblock = 0; superblock < 4; ++superblock) {
        for (unsigned block = 0; block < 4; ++block) {
            for (unsigned row_in_block = 0; row_in_block < 8; ++row_in_block) {
                unsigned effective_row = row_in_block + (block & 0x1 ? 8 : 0) + (superblock & 0x1 ? 16 : 0);
                spritemap[superblock][block][row_in_block] = (bitmap[effective_row] >> (block & 0x2 ? 0 : 8) >> (superblock & 0x2 ? 0 : 16)) & 0xFF;
            }
        }
    }
}

static uint16_t vgfx_map_half_cylinder_16_half_line(uint16_t val) {
    val >>= 8;

    val = (((val >> 2) & 0xF) << 4) | (((val >> 0) & 0x7) << 1) | (((val >> 0) & 0x1) << 0);

    val <<= 8;
    return val;
}

void vgfx_map_half_cylinder_16(uint16_t bitmap[16]) {
    for (unsigned line = 0; line < 16; ++line) {
        bitmap[line] = vgfx_map_half_cylinder_16_half_line(bitmap[line]) | breverse_u16(vgfx_map_half_cylinder_16_half_line(breverse_u16(bitmap[line])));
    }
}

static uint16_t vgfx_map_circle_16_half_line(uint16_t val, unsigned line) {
    if (line >= 8) {
        line = 15 - line;
    }
    val >>= 8;

    switch (line) {
    case 0:
        val = ((val >> 5) & 0x1) << 2 | ((val >> 3) & 0x1) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 1:
        val = ((val >> 5) & 0x3) << 3 | ((val >> 2) & 0x3) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 2:
        val = ((val >> 7) & 0x1) << 5 | ((val >> 3) & 0x7) << 2 | ((val >> 0) & 0x3) << 0;
        break;
    case 3:
    case 4:
        val = ((val >> 5) & 0x7) << 4 | ((val >> 0) & 0xF) << 0;
        break;
    default:
        break;
    }

    val <<= 8;
    return val;
}

void vgfx_map_circle_16(uint16_t bitmap[16]) {
    for (unsigned line = 0; line < 16; ++line) {
        bitmap[line] = vgfx_map_circle_16_half_line(bitmap[line], line) | breverse_u16(vgfx_map_circle_16_half_line(breverse_u16(bitmap[line]), line));
    }
}

void vgfx_map_half_sphere_16(uint16_t bitmap[16]) {
    vgfx_map_half_cylinder_16(bitmap);
    vgfx_map_circle_16(bitmap);
}

static uint32_t vgfx_map_half_cylinder_32_half_line(uint32_t val) {
    val >>= 16;

    val = (((val >> 12) & 0x1) << 15) | (((val >> 6) & 0x1F) << 10) | (((val >> 4) & 0x7) << 7) | (((val >> 2) & 0x7) << 4) | (((val >> 0) & 0x7) << 1) | (((val >> 0) & 0x1) << 0);

    val <<= 16;
    return val;
}

void vgfx_map_half_cylinder_32(uint32_t bitmap[32]) {
    for (unsigned line = 0; line < 32; ++line) {
        bitmap[line] = vgfx_map_half_cylinder_32_half_line(bitmap[line]) | breverse_u32(vgfx_map_half_cylinder_32_half_line(breverse_u32(bitmap[line])));
    }
}

static uint32_t vgfx_map_circle_32_half_line(uint32_t val, unsigned line) {
    if (line >= 16) {
        line = 31 - line;
    }
    val >>= 16;

    switch (line) {
    case 0:
        val = ((val >> 12) & 0x1) << 3 | ((val >> 8) & 0x1) << 2 | ((val >> 4) & 0x1) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 1:
        val = ((val >> 14) & 0x1) << 6 | ((val >> 11) & 0x1) << 5 | ((val >> 9) & 0x1) << 4 | ((val >> 7) & 0x1) << 3 | ((val >> 5) & 0x1) << 2 | ((val >> 2) & 0x1) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 2:
        val = ((val >> 14) & 0x1) << 8 | ((val >> 11) & 0x3) << 6 | ((val >> 9) & 0x1) << 5 | ((val >> 7) & 0x1) << 4 | ((val >> 4) & 0x3) << 2 | ((val >> 2) & 0x1) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 3:
        val = ((val >> 13) & 0x3) << 8 | ((val >> 10) & 0x3) << 6 | ((val >> 8) & 0x1) << 5 | ((val >> 5) & 0x3) << 3 | ((val >> 2) & 0x3) << 1 | ((val >> 0) & 0x1) << 0;
        break;
    case 4:
        val = ((val >> 15) & 0x1) << 10 | ((val >> 12) & 0x3) << 8 | ((val >> 9) & 0x3) << 6 | ((val >> 6) & 0x3) << 4 | ((val >> 3) & 0x3) << 2 | ((val >> 0) & 0x3) << 0;
        break;
    case 5:
        val = ((val >> 15) & 0x1) << 11 | ((val >> 11) & 0x7) << 8 | ((val >> 7) & 0x7) << 5 | ((val >> 3) & 0x7) << 2 | ((val >> 0) & 0x3) << 0;
        break;
    case 6:
        val = ((val >> 14) & 0x3) << 11 | ((val >> 9) & 0xF) << 7 | ((val >> 4) & 0xF) << 3 | ((val >> 0) & 0x7) << 0;
        break;
    case 7:
    case 8:
        val = ((val >> 13) & 0x7) << 11 | ((val >> 5) & 0x7F) << 4 | ((val >> 0) & 0xF) << 0;
        break;
    case 9:
    case 10:
    case 11:
        val = ((val >> 9) & 0x7F) << 8 | ((val >> 0) & 0xFF) << 0;
        break;
    default:
        break;
    }

    val <<= 16;
    return val;
}

void vgfx_map_circle_32(uint32_t bitmap[32]) {
    for (unsigned line = 0; line < 32; ++line) {
        bitmap[line] = vgfx_map_circle_32_half_line(bitmap[line], line) | breverse_u32(vgfx_map_circle_32_half_line(breverse_u32(bitmap[line]), line));
    }
}

void vgfx_map_half_sphere_32(uint32_t bitmap[32]) {
    vgfx_map_half_cylinder_32(bitmap);
    vgfx_map_circle_32(bitmap);
}
