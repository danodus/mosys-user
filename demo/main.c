#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

typedef unsigned long time_t;
typedef long suseconds_t;

struct timeval {
    time_t tv_sec;
    suseconds_t tv_usec;
};

struct input_event {
    struct timeval time;
    uint16_t type;
    uint16_t code;
    uint32_t value;
};

int main(int argc, char **argv) {
    const char *file = "/dev/input0";

    int fd = open(file, O_RDONLY);
    if (fd < 0) {
        err(EXIT_FAILURE, "Failed to open %s", file);
    }

    while (1) {
        struct input_event ev;
        ssize_t read_result = read(fd, &ev, sizeof(ev));
        if (read_result > 0) {
            printf("type %hu\n", ev.type);
            printf("code %hu\n", ev.code);
            printf("value %lu\n", ev.value);
        } else if (read_result == 0) {
            // Received no events
        } else {
            warn("Failed to read");
        }
    }
}
