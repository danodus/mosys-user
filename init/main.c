#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sched.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <spawn.h>
#include <mosys/reboot.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/syscall.h>
#include <sys/wait.h>

// #define ROOT_LOOP_FILE "root.img"
#define ROOT_DEVICE "/dev/mmcblk0p1"
#define ROOT_FSTYPE "fat"

#define EXIT_FAILURE_PID            2
#define EXIT_FAILURE_STDIO_OPEN     4

int main() {
    pid_t pid = getpid();
    if (pid != 1) {
        exit(EXIT_FAILURE_PID);
    }

    mount("", "/dev", "devfs", 0, NULL);
    mount("", "", "sockfs", 0, NULL);

    int stdin_fd = open("/dev/console", O_RDONLY);
    int stdout_fd = open("/dev/console", O_WRONLY);
    int stderr_fd = open("/dev/console", O_WRONLY);

    if (stdin_fd != 0   ||
        stdout_fd != 1  ||
        stderr_fd != 2) {
        const char *err_msg = "Unexpected I/O stream file descriptors, exiting\n";
        // Execution environment is not sane, this write might fail
        write(stderr_fd, err_msg, strlen(err_msg));
        exit(EXIT_FAILURE_STDIO_OPEN);
    }

    printf("Started `init'\n");


#ifdef ROOT_LOOP_FILE
    int root_img_fd = open(ROOT_LOOP_FILE, O_RDONLY);

    int loop0_fd = open(ROOT_DEVICE, O_RDONLY);
    if (loop0_fd >= 0) {
        ioctl(loop0_fd, 0, root_img_fd);
    }
#endif

    int mount_result = mount(ROOT_DEVICE, "/", ROOT_FSTYPE, 0, NULL);

    char *spawn_argv[] = {
        "sh",
        NULL
    };
    char *spawn_envp[] = {
        "PATH=/bin/",
        NULL
    };
    posix_spawn(NULL, "/bin/sh", NULL, NULL, spawn_argv, spawn_envp);

    while (1) {
        pid_t waited_pid = wait(NULL);
        if (waited_pid == -1 && errno == ECHILD) {
            // When init has no children left
            syscall(SYS_reboot, MOSYS_REBOOT_MAGIC1, MOSYS_REBOOT_MAGIC2, MOSYS_REBOOT_CMD_HALT, NULL);
        }
    }
}
