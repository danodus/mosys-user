#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <sched.h>
#include <stdlib.h>
#include <string.h>
#include <spawn.h>
#include <time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <sys/wait.h>

int main(int argc, char **argv, char **envp) {
    printf("argc: %d\n", argc);
    printf("argv (%p):\n", (void *) argv);
    for (char **argv_ptr = argv; *argv_ptr; argv_ptr += 1) {
        printf("    \"%s\"\n", *argv_ptr);
    }
    printf("envp (%p):\n", (void *) envp);
    for (char **envp_ptr = envp; *envp_ptr; envp_ptr += 1) {
        printf("    \"%s\"\n", *envp_ptr);
    }

    return EXIT_SUCCESS;
}
